
use std::{
	ops::{
		Not,
		Index,
		IndexMut,
		Add,
	},
	iter::successors,
	fmt::{self, Display, Formatter},
};

// first: boring stuff for representing the rules of chess

pub const A1: Pos = Pos(0, 0);
pub const A2: Pos = Pos(0, 1);
pub const A3: Pos = Pos(0, 2);
pub const A4: Pos = Pos(0, 3);
pub const A5: Pos = Pos(0, 4);
pub const A6: Pos = Pos(0, 5);
pub const A7: Pos = Pos(0, 6);
pub const A8: Pos = Pos(0, 7);
pub const B1: Pos = Pos(1, 0);
pub const B2: Pos = Pos(1, 1);
pub const B3: Pos = Pos(1, 2);
pub const B4: Pos = Pos(1, 3);
pub const B5: Pos = Pos(1, 4);
pub const B6: Pos = Pos(1, 5);
pub const B7: Pos = Pos(1, 6);
pub const B8: Pos = Pos(1, 7);
pub const C1: Pos = Pos(2, 0);
pub const C2: Pos = Pos(2, 1);
pub const C3: Pos = Pos(2, 2);
pub const C4: Pos = Pos(2, 3);
pub const C5: Pos = Pos(2, 4);
pub const C6: Pos = Pos(2, 5);
pub const C7: Pos = Pos(2, 6);
pub const C8: Pos = Pos(2, 7);
pub const D1: Pos = Pos(3, 0);
pub const D2: Pos = Pos(3, 1);
pub const D3: Pos = Pos(3, 2);
pub const D4: Pos = Pos(3, 3);
pub const D5: Pos = Pos(3, 4);
pub const D6: Pos = Pos(3, 5);
pub const D7: Pos = Pos(3, 6);
pub const D8: Pos = Pos(3, 7);
pub const E1: Pos = Pos(4, 0);
pub const E2: Pos = Pos(4, 1);
pub const E3: Pos = Pos(4, 2);
pub const E4: Pos = Pos(4, 3);
pub const E5: Pos = Pos(4, 4);
pub const E6: Pos = Pos(4, 5);
pub const E7: Pos = Pos(4, 6);
pub const E8: Pos = Pos(4, 7);
pub const F1: Pos = Pos(5, 0);
pub const F2: Pos = Pos(5, 1);
pub const F3: Pos = Pos(5, 2);
pub const F4: Pos = Pos(5, 3);
pub const F5: Pos = Pos(5, 4);
pub const F6: Pos = Pos(5, 5);
pub const F7: Pos = Pos(5, 6);
pub const F8: Pos = Pos(5, 7);
pub const G1: Pos = Pos(6, 0);
pub const G2: Pos = Pos(6, 1);
pub const G3: Pos = Pos(6, 2);
pub const G4: Pos = Pos(6, 3);
pub const G5: Pos = Pos(6, 4);
pub const G6: Pos = Pos(6, 5);
pub const G7: Pos = Pos(6, 6);
pub const G8: Pos = Pos(6, 7);
pub const H1: Pos = Pos(7, 0);
pub const H2: Pos = Pos(7, 1);
pub const H3: Pos = Pos(7, 2);
pub const H4: Pos = Pos(7, 3);
pub const H5: Pos = Pos(7, 4);
pub const H6: Pos = Pos(7, 5);
pub const H7: Pos = Pos(7, 6);
pub const H8: Pos = Pos(7, 7);


/// One of the categories of pieces.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Piece {
	King,
	Queen,
	Rook,
	Bishop,
	Knight,
	Pawn,
}

/// White or black.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Color {
	White,
	Black,
}

/// A location on the board. File, then rank. 0-indexed.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Pos(pub i8, pub i8);

/// Representation of what is on a particular square on the board.
pub type Square = Option<(Color, Piece)>;

/// All the pieces on a board.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Board(pub [[Square; 8]; 8]);

/// Represents a move.
#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ChessMove(pub Pos, pub Pos);


impl Not for Color {
	type Output =  Self;

	fn not(self) -> Self {
		match self {
			Color::White => Color::Black,
			Color::Black => Color::White,
		}
	}
}

impl Display for Pos {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		let files = ["a", "b", "c", "d", "e", "f", "g", "h"];
		let ranks = ["1", "2", "3", "4", "5", "6", "7", "8"];
		f.write_str(files[self.0 as usize])?;
		f.write_str(ranks[self.1 as usize])?;
		Ok(())
	}
}

impl Display for ChessMove {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{} to {}", self.0, self.1)
	}
}

impl Index<Pos> for Board {
	type Output = Square;

	fn index(&self, Pos(file, rank): Pos) -> &Square {
		&self.0[file as usize][rank as usize]
	}
}

impl IndexMut<Pos> for Board {
	fn index_mut(&mut self, Pos(file, rank): Pos) -> &mut Square {
		&mut self.0[file as usize][rank as usize]
	}
}

/// Default implementation is a chess board initial setup.
impl Default for Board {
	fn default() -> Self {
		use Color::*;
		use Piece::*;

		let mut board = Board([[None; 8]; 8]);
		let row = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook];
		for (file, piece) in row.into_iter().enumerate() {
			board[Pos(file as i8, 0)] = Some((White, piece));
			board[Pos(file as i8, 7)] = Some((Black, piece));
		}
		for file in 0..8 {
			board[Pos(file as i8, 1)] = Some((White, Pawn));
			board[Pos(file as i8, 6)] = Some((Black, Pawn));
		}
		board
	}
}

impl Add<(i8, i8)> for Pos {
	type Output = Self;

	fn add(self, delta: (i8, i8)) -> Self {
		Pos(self.0 + delta.0, self.1 + delta.1)
	}
}

impl Pos {
	/// Is this position actually on the board? If not, operations may panic.
	pub fn in_bounds(self) -> bool {
		self.0 >= 0 && self.0 < 8 && self.1 >= 0 && self.1 < 8
	}

	/// Add some delta, unless that would trail off the board.
	pub fn try_add(self, delta: (i8, i8)) -> Option<Pos> {
		let output = self + delta;
		if output.in_bounds() {
			Some(output)
		} else {
			None
		}
	}
}

impl Board {
	/// Iterate all possible moves for the player of the given color to make in this
	/// scenario. This does not eagerly detect checkmate, and as such, may produce
	/// moves which are illegal on account of placing the player's king in check.
	/// Relatedly, if given a board in which the player who's turn it isn't is in check,
	/// this will produce moves that involve taking the king, which would typically
	/// be unreachable.
	pub fn chess_moves<'s>(
		&'s self,
		player: Color,
	) -> impl Iterator<Item=ChessMove> + 's {
		fn one_dist<'a, D: IntoIterator<Item=(i8, i8)> + 'static>(
			deltas: D,
			board: &'a Board,
			pos: Pos,
			player: Color,
		) -> impl Iterator<Item=Pos> + 'a {
			deltas
				.into_iter()
				.filter_map(move |delta| pos.try_add(delta))
				.filter(move |&dst| board[dst].map(|(color, _)| color) != Some(player))
		}

		fn any_dist<'a, D: IntoIterator<Item=(i8, i8)> + 'static>(
			deltas: D,
			board: &'a Board,
			pos: Pos,
			player: Color,
		) -> impl Iterator<Item=Pos> + 'a {
			deltas
				.into_iter()
				.flat_map(move |delta| successors(
					Some((pos, false)),
					move |&(curr, taken)| if taken {
						None
					} else {
						curr.try_add(delta)
							.and_then(|dst| match board[dst] {
								Some((color, _)) => if color == player {
									None
								} else {
									Some((dst, true))
								},
								_ => Some((dst, false)),
							})
					}
				).skip(1))
				.map(|(dst, _)| dst)
		}

		let straight_deltas = [(1, 0), (-1, 0), (0, 1), (0, -1)].iter().copied();
		let diagonal_deltas = [(1, 1), (-1, 1), (-1, -1), (1, -1)].iter().copied();
		let omnidir_deltas = straight_deltas.clone().chain(diagonal_deltas.clone());

		let player_pieces = (0..8)
			.flat_map(|rank| (0..8).map(move |file| Pos(file, rank)))
			.filter_map(move |pos| self[pos]
				.filter(move |&(color, _)| color == player)
				.map(move |(_, piece)| (piece, pos)));

		macro_rules! iterplex {
			($($variant:ident),*)=>{
				enum Iterplex<$( $variant: Iterator<Item=Pos> ),*> {
					$( $variant($variant) ),*
				}
				impl<
					$( $variant: Iterator<Item=Pos> ),*
				> Iterator for Iterplex<$( $variant ),*> {
					type Item = Pos;
					fn next(&mut self) -> Option<Pos> {
						match self {
							$( &mut Iterplex::$variant(ref mut iter) => iter.next(), )*
						}
					}
				}
			};
		}
		iterplex!(V1, V2, V3, V4, V5, V6);

		player_pieces.flat_map(move |(piece, pos)| {
			let dsts = match piece {
				Piece::King => Iterplex::V1(
					one_dist(omnidir_deltas.clone(), self, pos, player)
					// TODO castling
				),
				Piece::Queen => Iterplex::V2(
					any_dist(omnidir_deltas.clone(), self, pos, player)
				),
				Piece::Rook => Iterplex::V3(
					any_dist(straight_deltas.clone(), self, pos, player)
				),
				Piece::Bishop => Iterplex::V4(
					any_dist(diagonal_deltas.clone(), self, pos, player)
				),
				Piece::Knight => Iterplex::V5({
					let coefficients = [(1, 2), (2, 1)].iter().copied();
					let deltas = diagonal_deltas.clone()
						.flat_map(move |(d1, d2)| coefficients.clone()
							.map(move |(c1, c2)| (c1 * d1, c2 * d2)));
					one_dist(deltas, self, pos, player)
				}),
				Piece::Pawn => Iterplex::V6({
					let (dir, start_rank) = match player {
						Color::White => (1, 1),
						Color::Black => (-1, 6),
					};

					let advance1 = pos
						.try_add((0, dir))
						.filter(|&dst| self[dst].is_none());

					let advance2 = if pos.1 == start_rank {
						let dst = pos + (0, dir * 2);
						if self[dst].is_none() {
							Some(dst)
						} else {
							None
						}
					} else {
						None
					};

					let takes = [-1, 1].into_iter()
						.map(move |strafe| (strafe, dir))
						.filter_map(move |delta| pos.try_add(delta))
						.filter(move |&dst| self[dst].map(|(color, _)| color) == Some(!player));

					advance1.into_iter()
						.chain(advance2)
						.chain(takes)
				}),
			};
			dsts.map(move |dst| ChessMove(pos, dst))
		})
	}

	/// Apply a move to self.
	pub fn apply(&mut self, chess_move: ChessMove) {
		self[chess_move.1] = self[chess_move.0];
		self[chess_move.0] = None;
	}

	/// Apply a move to self.
	#[must_use]
	pub fn applied(mut self, chess_move: ChessMove) -> Self {
		self.apply(chess_move);
		self
	}

	/// Does the move involve taking a king.
	pub fn ends_game(&self, chess_move: ChessMove) -> bool {
		self[chess_move.1].map(|(_, piece)| piece) == Some(Piece::King)
	}

	pub fn print(&self) {
		print!("┌");
		for file in 0..8 {
			print!("──");
			if file + 1 < 8 {
				print!("┬");
			}
		}
		println!("┐");
		for rank in (0..8).rev() {
			print!("│");
			for file in 0..8 {
				if let Some((color, piece)) = self[Pos(file, rank)] {
					use Color::*;
					use Piece::*;
					print!("{}", match color {
						White => "W",
						Black => "B",
					});
					print!("{}", match piece {
						King => "K",
						Queen => "q",
						Rook => "r",
						Bishop => "b",
						Knight => "k",
						Pawn => "p",
					});
				} else {
					print!("  ");
				}
				print!("│");
			}
			println!();
			if rank > 0 {
				print!("├");
				for file in 0..8 {
					print!("──");
					if file + 1 < 8 {
						print!("┼");
					}
				}
				println!("┤");
			}
		}
		print!("└");
		for file in 0..8 {
			print!("──");
			if file + 1 < 8 {
				print!("┴");
			}
		}
		println!("┘");
	}
}


pub fn decide_move(board: &Board, player: Color) -> ChessMove {
	fn f(
		board: Board,
		player: Color,
		recursions: u32,
		push_to: &mut Vec<Board>,
	) -> (bool, bool) {
		//println!("winding up, {} recursions remaining", recursions);
		let output = if recursions == 0 {
			push_to.push(board);
			(false, false)
		} else {
			let mut player_win = false;
			let mut enemy_win = true;
			for chess_move in board.chess_moves(player) {
				if board.ends_game(chess_move) {
					player_win = true;
					enemy_win = false;
				} else {
					let board2 = board.applied(chess_move);
					let (causes_enemy_win, causes_player_win) = f(
						board2,
						!player,
						recursions - 1,
						push_to,
					);
					player_win |= causes_player_win;
					enemy_win &= causes_enemy_win;
				}
			}
			(player_win, enemy_win)
		};
		//println!("winding down, {} recursions remaining", recursions);
		output
	}

	#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
	struct Score(i8, u64);

	let mut states = Vec::new();
	let best_move = board
		.chess_moves(player)
		.max_by_key(|&chess_move| {
			//println!("considering {}", chess_move);
			let board2 = board.applied(chess_move);
			states.clear();
			let (causes_enemy_win, causes_player_win) = f(
				board2,
				!player,
				3,
				&mut states,
			);
			let guaranteedness = match (causes_player_win, causes_enemy_win) {
				(true, false) => 1,
				(false, false) => 0,
				(false, true) => -1,
				(true, true) => unreachable!(),
			};
			let score = Score(guaranteedness, states.len() as u64);
			//println!("{:?}", score);
			score
		})
		.unwrap();
	best_move
}


pub fn decide_move2(board: &Board, player: Color) -> ChessMove {
	#[derive(Debug, Copy, Clone, Default)]
	struct Eliminations([u8; 6])

	fn f(
		board: Board,
		player: Player,
		recursions: u32,
	) -> (Eliminations, Eliminations) {
		if recursions == 0 {
			(
				Eliminations::default(),
				Eliminations::default(),
			)
		} else {
			let mut a = None;
			let mut b = None;
			for chess_move in board.chess_moves(player) {
				if let Some((_, piece)) = board[chess_move.1] {

					b.0[piece as usize] += 1;
				}
			}
		}
	}
	*/
}


fn main() {
    let mut board = Board::default();
    let mut player = Color::White;
    //board.print();
    //return;
    /*for possible in board.chess_moves(player) {
    	println!("{}", possible);
    }
    return;*/
    loop /*for _ in 0..20*/ {
    	//println!("{:?} thinking", player);
    	board.print();
    	//let player_move = decide_move(&board, player);
    	let player_move = {
    		if player == Color::White {
    			decide_move(&board, player)
    		} else {
    			board.chess_moves(player).next().unwrap()
    		}
    	};
    	println!("{:?}: {}", player, player_move);
    	if board.ends_game(player_move) {
    		println!("{:?} wins", player);
    		break;
    	} else {
    		board.apply(player_move);
    		player = !player;
    	}
    }
}
